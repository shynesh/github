/*
 * CtrlStmntSwitch.c
 *
 *  Created on: Dec 5, 2020
 *      Author: narima
 */

/* Write a function escape(s,t) that converts characters like newline and tab into visible escape sequences like \n and \t
 *  as it copies the string t to s. USe a switch. Write a function for the other direction as well, converting escape
 *  sequences into the real characters.
 */

#include <stdio.h>
#define N 100
void escape(char *, char *);
//extern char s[N];
//extern char t[N];
int main()
{
	char *t;
	char *s="shynesh\nsukumaran\n";
	escape(s,t);

	printf("\n%s",s);
	return 0;
}

void escape(char *s, char *t)
{
	int i,j;
	printf("%s",s);
	j=0;
	for (i=0;s[i]!='\0';i++)
	{

		switch(s[i])
		{
		case '\n':
			t[j]='\\';
			j++;
			t[j]='\\n';
			break;
		case '\t':
			t[j]='\\';'
			t[j]='\\t';
			break;
		default:
			t[j]=s[i];
		}
		j++;
	}
	//j++;
	t[j]='\0';
	printf("%s",t);
	return 0;
}
