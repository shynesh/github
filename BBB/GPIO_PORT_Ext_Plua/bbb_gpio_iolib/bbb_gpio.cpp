/*
 * bbb_gpio.cpp
 *
 *  Created on: Jul 14, 2024
 *      Author: narima
 */

#include <iostream>
#include "gpio.h"

int main()
{
	unsigned int pin = 27;
	gpio_export(pin);
	gpio_set_dir(pin,OUT);
	gpio_set_value(pin,1);
}


