/*
 * led_on.h
 *
 *  Created on: Jul 12, 2024
 *      Author: narima
 */

#ifndef LED_ON_H_
#define LED_ON_H_

#define LED3_PATH "/sys/class/leds/beaglebone:green:usr3"

void writeLED(char filename[], char value[]);
void removeTrigger();

#endif /* LED_ON_H_ */
