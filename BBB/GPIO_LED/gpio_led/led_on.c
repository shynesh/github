/*
 * led_on.c
 *
 *  Created on: Jul 12, 2024
 *      Author: narima
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "led_on.h"
#define LED3_PATH "/sys/class/leds/beaglebone:green:usr3"

void writeLED(char filename[], char value[])
{
	FILE* fp;
	char fullFileName[100];
	sprintf(fullFileName, LED3_PATH "%s",filename);
	fp=fopen(fullFileName, "w+");
	fprintf(fp,"%s",value);
	fclose(fp);
}

void removeTrigger()
{
	writeLED("/trigger","none");
}
