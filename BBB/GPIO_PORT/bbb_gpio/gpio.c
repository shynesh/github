/*
 * gpio.c
 *
 *  Created on: Jul 14, 2024
 *      Author: narima
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define GPIO_PATH "/sys/class/gpio"
#define GPIO44_PATH "/sys/class/gpio/gpio44"
#define GPIO45_PATH "/sys/class/gpio/gpio45"

int main()
{
	enableGPIO("/export","44");
	writeGPIO("/direction","out");
	writeGPIO("/value","0");
}

void enableGPIO(char gpioCommand[],char gpioChannel[])
{
	FILE* fp;
	char fullFileName[100];
	sprintf(fullFileName, GPIO_PATH "%s",gpioCommand);
	fp=fopen(fullFileName,"w+");
	fprintf(fp,"%s",gpioChannel);
	fclose(fp);
}
void writeGPIO(char gpioCommand[],char value[])
{
	FILE* fp;
	char fullFileName[100];
	sprintf(fullFileName, GPIO44_PATH "%s",gpioCommand);
	fp=fopen(fullFileName,"w+");
	fprintf(fp,"%s",value);
	fclose(fp);
}

