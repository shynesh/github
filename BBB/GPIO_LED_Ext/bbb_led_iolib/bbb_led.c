/*
 * bbb_led.c
 *
 *  Created on: Jul 13, 2024
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "gpio.h"
#include "am335x.h"

int main(void)
{

	digitalWrite(USR3, HIGH);
	while(1)
	{
		digitalWrite(P8_12, LOW);
		usleep(500000);
		digitalWrite(P8_12, HIGH);
		usleep(500000);
	}
}
