/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdbool.h>
#include "Function.h"

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node on the front of the list */
void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node*));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* Check whether the value x is present in linked list */
bool search(struct Node* head, int x)
{
	struct Node* current = head; // Initialize current
	while(current != NULL)
	{
		if (current->data == x)
			return true;
		current = current->next;
	}
	return false;
}
