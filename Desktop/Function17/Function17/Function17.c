/*
 * Function17.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Function.h"

int main()
{
	/* Start with the empty list */
	struct Node* head = NULL;
	int x = 21;

	/* Use push() to construct below list */
	/* 14->21->11->30->10 */
	push(&head, 10);
	push(&head, 30);
	push(&head, 11);
	push(&head, 21);
	push(&head, 14);

	search(head, 21)?printf("Yes") : printf("No");
	return 0;
}
