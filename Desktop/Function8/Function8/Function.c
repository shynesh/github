/*
 * Function.c
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "Function.h"

const char* getfield(char* line, int num)
{
	const char* tok;
	//for (tok = strtok(line, ";");tok && *tok; tok = strtok(NULL, ";\n"))
	for (tok = strtok(line, ",");tok && *tok; tok = strtok(NULL, ",\n"))
	{
		if (!--num)
		{
			return tok;
		}
	}
	return NULL;
}

int readCsv(char *filename,int colNum)
{
	FILE* stream = fopen(filename, "r");
	char line[1024];
	while (fgets(line, 1024, stream))
	{
		char* tmp = strdup(line);
		printf("Field %d would be %s\n",colNum,getfield(tmp,colNum));
		// NOTE strtok clobbers tmp
		free(tmp);
	}
	return 1;
}
