/*
 * Structure4.c
 *
 *  Created on: Dec 13, 2021
 *      Author: narima
 */


#include <stdio.h>
#include <ctype.h>
#include <string.h>
//#include <curses.h>
#include <termios.h>

#define MAXWORD 100
#define NKEYS 100

struct key {
	char *word;
	int count;
} keytab[]= {{"auto",0},{"break",0},{"case",0},{"char",0},{"const",0},{"continue",0},{"default",0}};

//int getword(char *, int);
static struct termios old, current;

/* Initialize new terminal i/o settings */
void initTermios(int echo)
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  current = old; /* make new settings same as old settings */
  current.c_lflag &= ~ICANON; /* disable buffered i/o */
  if (echo) {
      current.c_lflag |= ECHO; /* set echo mode */
  } else {
      current.c_lflag &= ~ECHO; /* set no echo mode */
  }
  tcsetattr(0, TCSANOW, &current); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void)
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo)
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void)
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void)
{
  return getch_(1);
}


//int binsearch(char *, struct key *, int);
/* BINSEARCH function */
/* Find word in tab[0] ...... tab[n-1] */

int binsearch (char *word, struct key tab[], int n)
{
	int cond;
	int low, high, mid;

	low = 0;
	high = n-1;
	while(low <= high) {
		mid = (low+high)/2;
		if ((cond = strcmp(word, tab[mid].word)) < 0)
		{
			high = mid - 1;
		}
		else if (cond > 0)
		{
			low = mid +1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}

/* GETWORD function */
/* get next word or character from input */
int getword (char *word, int lim)
{
	int c;
	//int ungetch(int);
	char *w = word;

	while(isspace(c = getchar()))
	{
		;
	}
	if (c != '.')
	{
		*w++ = c;
	}
	if (!isalpha(c))
	{
		*w = '\0';
		return c;
	}
	for (; --lim > 0; w++)
	{
		if (!isalnum(*w = getchar()))
		{
			//ungetch(*w);
			break;
		}
	}
	*w = '\0';
	return word[0];
}



/* count C keywords */

int main ()
{
	int n;
	char word[MAXWORD];

	while(getword(word, MAXWORD) != EOF)
	{
		if (isalpha(word[0]))
		{
			if ((n == binsearch(word, keytab, NKEYS)) >= 0)
			{
				keytab[n].count++;
			}
		}
	}
	for (n = 0; n < NKEYS; n++)
	{
		if (keytab[n].count > 0)
		{
			printf("%4d %s\n",keytab[n].count, keytab[n].word);
		}
	}
	return 0;
}



