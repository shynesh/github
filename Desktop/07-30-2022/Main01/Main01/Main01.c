/*
 * Main01.c
 *
 *  Created on: Jul 30, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <Arduino_FreeRTOS.h>

int main(int argc, char *argv[])
{
	int i=0;
	int value;
	char CharValue;
	char Data[8];
	int count=0;
	struct point{
		int x;
		int y;
	}pt1,pt2;
	struct midPoint{
		float x;
		float y;
	}mid;
	unsigned int stBracket = 0;
	for (i=1;i<argc; i++)
	{
		//printf("%s\n",argv[i]);
		if (strlen(argv[i])>4)
		{
			if ((argv[i][0]=='[') && (argv[i][4]==']') && (argv[i][2]==','))
			{
				CharValue=argv[i][1];
				value=CharValue-'0';
				Data[count]=value;
				if (count == 0)
				{
					pt1.x=value;
				}
				else if(count == 2)
				{
					pt2.x=value;
				}
				count++;
//				printf("%d\n",value);
				CharValue=argv[i][3];
				value=CharValue-'0';
				Data[count]=value;
				if (count == 1)
				{
					pt1.y=value;
				}
				else if(count == 3)
				{
					pt2.y=value;
				}
				count++;
//				printf("%d\n",value);
			}
			else
			{
				;
			}
		}
		else
		{
			if ((argv[i][0]=='[')&&(stBracket==0))
			{
				stBracket=1;
			}
			else if ((argv[i][0]==']')&&(stBracket==1))
			{
				stBracket=2;
			}
			else if ((argv[i][0]=='[')&&(stBracket==2))
			{
				stBracket=3;
			}
			else if ((argv[i][0]==']'&&(stBracket==3)))
			{
				stBracket=4;
			}
			else if ((argv[i][0]!=',')&&(stBracket==1))
			{
				CharValue=argv[i][0];
				value=CharValue-'0';
				Data[count]=value;
				if (count == 0)
				{
					pt1.x=value;
				}
				else if (count == 2)
				{
					pt2.x=value;
				}
				count++;
//				printf("%d\n",value);
			}
			else if ((argv[i][0]!=',')&&(stBracket==3))
			{
				CharValue=argv[i][0];
				value=CharValue-'0';
				Data[count]=value;
				if (count == 1)
				{
					pt1.y=value;
				}
				else if (count == 3)
				{
					pt2.y=value;
				}
				count++;
//				printf("%d\n",value);
			}
			else if ((argv[i][0]==','))
			{
				;
			}
			else
			{
				CharValue=argv[i][0];
				value=CharValue-'0';
				Data[count]=value;
				if (count == 0)
				{
					pt1.x=value;
				}
				else if (count == 1)
				{
					pt1.y=value;
				}
				else if (count == 2)
				{
					pt2.x=value;
				}
				else if (count == 3)
				{
					pt2.y=value;
				}
				count++;
//				printf("%d\n",value);
			}
		}

	}
	printf("-------------------\n");
	for (i=0;i<count;i++)
	{
		printf("%d\n",Data[i]);
	}
	printf("-------------------\n");
	printf("%d\n",pt1.x);
	printf("%d\n",pt1.y);
	printf("%d\n",pt2.x);
	printf("%d\n",pt2.y);
	mid.x = (float)(((pt1.x))+((pt2.x)))/2;
	mid.y = (float)(((pt1.y))+((pt2.y)))/2;
	printf("Middle Point = (%f,%f)\n",mid.x,mid.y);
	return 0;
}
