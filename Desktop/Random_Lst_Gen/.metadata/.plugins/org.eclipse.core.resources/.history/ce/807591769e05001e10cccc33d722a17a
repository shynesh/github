/*
 * main.c
 *
 *  Created on: Jun 6, 2023
 *      Author: narima
 */

// Random List Generator Program
// Sort the list

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#define RANGE 100
#define POPULATION 100
//#define NULL

typedef struct list {int data; struct list *next;}list;

int is_empty(const list *l){return (l==NULL);}

list *create_list(int d)
{
	list *head = malloc(sizeof(list));
	head->data = d;
	head->next = NULL;
	return head;
}

list *add_to_front(int d, list *h)
{
	list *head = create_list(d);
	head->next = h;
	return head;
}

list *rand_num_list()
{
	int i =0;
	int weight;
	srand(time(0));
	weight = rand()%RANGE + RANGE;
	list *head = create_list(weight);
	for (i=0;i<POPULATION-1;i++)
	{
		weight = rand()%RANGE + RANGE;
		head = add_to_front(weight,head);
	}
	return head;
}

void print_list(list *h, char *title)
{
	printf("%s\n",title);
	while(h!=NULL)
	{
		printf("%d :",h->data);
		h=h->next;
	}
}

int getCount(const list *h)
{
	int count = 0;
	while(h!=NULL)
	{
		count++;
		h = h->next;
	}
	return count;
}

void swapTwoElement(list **head)
{
	if (*head == NULL || (*head)->next == NULL){
		return;
	}
	// No need to swap if the list is empty

	list *prev = *head;
	list *curr = (*head)->next;

	// Swap the first two
	*head = curr;
	prev->next = curr->next;
	curr->next = prev;

	//Traverse the rest of the list and swap node pairwise
	while(prev->next != NULL && prev->next->next != NULL)
	{
		list *next1 = prev->next;
		list *next2 = prev->next->next;

		prev->next = next2;
		next1->next = next2->next;
		next2->next = next1;

		prev = next1;
	}
}

void swap(list *a, list *b)
{
	int temp = a->data;
	a->data = b->data;
	b->data = temp;
}

void sort(list *head)
{
	int status;
	list *next1;
	list *next2 = NULL;

	do{
		status = 0;
		next1 = head;
		while(next1->next != next2)
		{
			if(next1->data > next1->next->data)
			{
				swap(next1,next1->next);
				status = 1;
			}next1 = next1->next;
		}next2 = next1;
	}while(status);
}

int main()
{
	list *head = NULL;
	list *temp;
	list *prev;
	int i,j;
	head = rand_num_list();
	print_list(head,"Random Integer Number set");
	printf("\n\n");
	printf("Linked List Count = %d\n",getCount(head));

	swapTwoElement(&head);
	print_list(head,"Random Integer Number set");
	printf("\n");
	printf("Linked List Count = %d\n",getCount(head));

	sort(head);
	print_list(head,"Random Integer Number set");
	printf("\n");
	printf("Linked List Count = %d\n",getCount(head));

	printf("\n\n");
	return 0;
}
