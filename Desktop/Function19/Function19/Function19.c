/*
 * Function19.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "Function.h"

int main()
{
	struct Node* head = NULL;
	// Use push to construct below list
	// 1->12->1->4->1
	push(&head, 1);
	push(&head, 12);
	push(&head, 1);
	push(&head, 4);
	push(&head, 1);

	// Check the count Function
	printf("Element at index 3 is %d", GetNth(head, 3));
	return 0;
}
