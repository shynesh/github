/*
 * LinkedList8.c
 *
 *  Created on: Jan 15, 2022
 *      Author: narima
 */

// Iterative C program to search an element in linked list
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// A linked list node
struct Node {
	int data;
	struct Node* next;
};

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node on the front of the list */
void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node*));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* Check whether the value x is present in linked list */
bool search(struct Node* head, int x)
{
	struct Node* current = head; // Initialize current
	while(current != NULL)
	{
		if (current->data == x)
			return true;
		current = current->next;
	}
	return false;
}

/* Driver program to test count function */
int main()
{
	/* Start with the empty list */
	struct Node* head = NULL;
	int x = 21;

	/* Use push() to construct below list */
	/* 14->21->11->30->10 */
	push(&head, 10);
	push(&head, 30);
	push(&head, 11);
	push(&head, 21);
	push(&head, 14);

	search(head, 21)?printf("Yes") : printf("No");
	return 0;
}
