/*
 * Function.c
 *
 *  Created on: Nov 6, 2021
 *      Author: narima
 */

#include "Function.h"

Swap1(int *a, int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}
