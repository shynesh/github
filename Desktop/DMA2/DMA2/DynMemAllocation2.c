/*
 * DynMemAllocation2.c
 *
 *  Created on: Jul 2, 2022
 *      Author: narima
 */

#include<stdio.h>
#include <stdlib.h>

int main()
{
	struct Node{
		int a;
		unsigned char num;
		struct Node* next;
	};

	struct Store{
		int b;
		unsigned char num2;
	};

	struct Node* head = NULL;
	struct Node* first = NULL;
	head = (struct Node*)malloc(sizeof(struct Node));
	first = (struct Node*)malloc(sizeof(struct Node));
	struct Store *data1;
	head->a=1;
	head->num=12;
	head->next=first;
	first->a=2;
	first->num=13;
	first->next=NULL;
	(*data1).b = 1;
	(*data1).num2 = 14;
}
