/*
 * Function14.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "Function.h"

int main()
{
	/* Start with the empty list */

	struct Node *head = NULL;

	push(&head, 7);
	push(&head, 1);
	push(&head, 3);
	push(&head, 2);
	push(&head, 8);

	puts("Created Linked List: ");
	printList(head);
	deleteNode(&head, 4);
	puts("\nLinked List after Deletion at position 4: ");
	printList(head);
	return 0;
}
