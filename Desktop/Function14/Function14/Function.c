/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include "Function.h"

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node in front of the list */

void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}
/* Given a reference (pointer to pointer) to the head of a list and a position, deletes the node at the given position */

void deleteNode(struct Node** head_ref, int position)
{
	// If linked list is empty
	if (*head_ref == NULL)
		return;

	// Store head node
	struct Node* temp = *head_ref;

	// If head needs to be removed
	if (position == 0){
		*head_ref = temp->next; // Changed head
		free(temp); // free old head
		return;
	}

	// Find previous node of the node to be deleted
	for (int i = 0; temp != NULL && i < position - 1; i++)
		temp = temp->next;

	// if the position is more than number of nodes
	if (temp == NULL || temp->next == NULL)
		return;

	// node temp->next is the node to be deleted
	// Store pointer to the next of node to be deleted

	struct Node* next = temp->next->next;

	// unlink the node from linked list
	free(temp->next); // Free memory

	temp->next = next;
}

/* This function prints contents of linked list starting from the given node */
void printList(struct Node* n)
{
	while (n != NULL){
		printf(" %d ",n->data);
		n=n->next;
	}
}
