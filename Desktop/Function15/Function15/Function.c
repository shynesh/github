/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <assert.h>
#include "Function.h"

// Function to delete the entire linked list
void deleteList(struct Node** head_ref)
{
	/* deref head_ref to get the real head */
	struct Node* current = *head_ref;
	struct Node* next;

	while(current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	/* deref head_ref to affect the real head back in the caller. */
	*head_ref = NULL;
}

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node in front of the list */

void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* This function prints contents of linked list starting from the given node */
void printList(struct Node* n)
{
	while (n != NULL){
		printf(" %d ",n->data);
		n=n->next;
	}
}
