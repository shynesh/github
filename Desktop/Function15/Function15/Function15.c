/*
 * Function15.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Function.h"

int main()
{
	struct Node* head = NULL;
	push(&head, 1);
	push(&head, 4);
	push(&head, 1);
	push(&head, 12);
	push(&head, 1);
	printList(head);
	printf("\n Deleting linked list");
	deleteList(&head);
	printf("\n Linked List deleted");
	printList(head);
}

