/*
 * Structure3.c
 *
 *  Created on: Nov 7, 2021
 *      Author: narima
 */

#include<stdio.h>

struct point {int x; int y;};
struct point makePoint(int x, int y);
struct point getMiddlePoint(struct point, struct point);

int main()
{
	struct point pt1,pt2,middle;
	pt1=makePoint(1,1);
	pt2=makePoint(3,3);
	middle=getMiddlePoint(pt1,pt2);
}

struct point getMiddlePoint(struct point Pt1, struct point Pt2)
{
	struct point temp;
	temp.x=(Pt1.x+Pt2.x)/2;
	temp.y=(Pt1.y+Pt2.y)/2;
	return temp;
}

struct point makePoint(int X, int Y)
{
	struct point Temp;
	Temp.x=X;
	Temp.y=Y;
	return Temp;
}
