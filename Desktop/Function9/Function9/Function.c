/*
 * Function.c
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<libxml/xmlreader.h>
#include<libxml/xmlmemory.h>
#include<libxml/parser.h>
#include "Function.h"


int readXml(char *filename,char configReceive[7][80])
{
	char *docname;
	xmlDocPtr doc;
	xmlNodePtr cur;
	xmlChar *uri;
	char config[7][80] = {"author", "title", "genre", "price", "publish_date", "description", "status"};
	//char config[7][80] = {"author", "title", "genre", "price", "publish_date", "description"};
	int count = 0;
	int count1 = 0;
	docname = filename;
	doc = xmlParseFile(docname);
	cur = xmlDocGetRootElement(doc);
	cur = cur->xmlChildrenNode;
	while(cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"xmlConfig")))
		{
			uri = xmlGetProp(cur, (xmlChar *)config[count++]);
			strcpy(configReceive[count1++], (char *)uri);
			xmlFree(uri);
		}
		cur = cur->next;
	}
	count = 0;
	count1 = 0;
	xmlFreeDoc(doc);
	return 0;
}
