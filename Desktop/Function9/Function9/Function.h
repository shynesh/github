/*
 * Function.h
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_


#ifndef BOOK_VALUE
#define BOOK_VALUE

extern struct bookData {
	char *author;
	char *title;
	char *genre;
	char *price;
	char *publish_date;
	char *description;
};

struct myBook {
	struct bookData *MyBook;
};
#endif

int readXml(char *filename,char configReceive[7][80]);

#endif /* FUNCTION_H_ */
