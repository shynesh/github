/*
 * Function.h
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

int is_leaf(xmlNode * node);
void print_xml(xmlNode * node, int indent_len);


#endif /* FUNCTION_H_ */
