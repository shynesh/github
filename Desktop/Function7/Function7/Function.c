/*
 * Function.c
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include "Function.h"

//char rxData[5][20] = {" "};
int is_leaf(xmlNode * node)
{
	xmlNode * child = node->children;
	while(child)
	{
		if(child->type == XML_ELEMENT_NODE) return 0;
		child = child->next;
	}
	return 1;
}

void print_xml(xmlNode * node, int indent_len)
{
	int i=0;
	while(node)
	{
		if((node->type == XML_ELEMENT_NODE)&&(is_leaf(node)==1))
		{
			if (strcmp(node->name,"author")==0)
			{
				printf("%s:%s\n", node->name,xmlNodeGetContent(node));
				//strcpy(rxData[i++],xmlNodeGetContent(node));
			}
			else if (strcmp(node->name,"title")==0)
			{
				printf("%s:%s\n", node->name,xmlNodeGetContent(node));
			}
			else
			{
				;
			}
		}
		print_xml(node->children, indent_len + 1);
		node = node->next;
	}
}

