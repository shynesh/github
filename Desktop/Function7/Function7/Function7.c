/*
 * Function7.c
 *
 *  Created on: Jan 1, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <libxml/parser.h>
#include "Function.h"

int main()
{
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;

	doc = xmlReadFile("sampleXml.xml", NULL, 0);
	if (doc == NULL) {
		printf("Could not parse the XML file");
	}
	root_element = xmlDocGetRootElement(doc);
	print_xml(root_element, 1);
	xmlFreeDoc(doc);
	xmlCleanupParser();
}
