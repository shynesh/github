/*
 * LinkedList6.c
 *
 *  Created on: Jan 15, 2022
 *      Author: narima
 */

// C program to delete a linked list

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// A linked list node

struct Node {
	int data;
	struct Node * next;
};

// Function to delete the entire linked list
void deleteList(struct Node** head_ref)
{
	/* deref head_ref to get the real head */
	struct Node* current = *head_ref;
	struct Node* next;

	while(current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	/* deref head_ref to affect the real head back in the caller. */
	*head_ref = NULL;
}

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node in front of the list */

void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

// Driver program to test count function

int main()
{
	struct Node* head = NULL;
	push(&head, 1);
	push(&head, 4);
	push(&head, 1);
	push(&head, 12);
	push(&head, 1);
	printf("\n Deleting linked list");
	deleteList(&head);
	printf("\n Linked List deleted");
}
