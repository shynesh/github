/*
 * Function.c
 *
 *  Created on: Nov 7, 2021
 *      Author: narima
 */
#include "Function.h"
#include <math.h>

struct point makePoint(int x, int y)
{
	struct point temp;

	temp.x=x;
	temp.y=y;
	return temp;
}

struct point middlePoint(struct point pt1, struct point pt2)
{
	struct point MiddlePoint;
	MiddlePoint=makePoint((pt1.x+pt2.x)/2,(pt1.y+pt2.y)/2);
	return MiddlePoint;
}

double distancePoint(struct point pt1, struct point pt2)
{
	double dist;
	dist = sqrt((double)(pt2.x-pt1.x)*(pt2.x-pt1.x)+(double)(pt2.y-pt1.y)*(pt2.y-pt1.y));
	return dist;
}
