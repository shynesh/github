/*
 * Function.h
 *
 *  Created on: Nov 7, 2021
 *      Author: narima
 *  So in the Header file, we need to declare both Function as well as the new Structure data type,
 *  The Structure data type is called "point"
 *  The new function is called "makePoint", we can see that the return value of the function is "point"
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

#ifndef POINT_VALUE
#define POINT_VALUE

struct point {
	int x;
	int y;
};
#endif

struct point makePoint(int, int);
struct point middlePoint(struct point, struct point);
double distancePoint(struct point, struct point);

#endif /* FUNCTION_H_ */
