/*
 * Function3.c
 *
 *  Created on: Nov 7, 2021
 *      Author: narima
 *  This function,main function written in Function3.c file, is calling another function, makePoint, written in another file, Function.c. The definition and deceleration is written in the Header
 *  file called Function.h
 */

#include <stdio.h>
#include "Function.h"

struct point main()
{
	struct point pt1,pt2,MiddlePoint;
	struct point origin, *pp;
	struct info {
		int len;
		char *str;
	};
	struct info my_info = {1,"Tested",2,"Tested"};
	struct info *p;
	p=&my_info;
	double dist;
	int x0,y0;

	pp = &origin;
	origin = makePoint(0,0);
	pt1=makePoint(1,1);
	pt2=makePoint(2,2);
	MiddlePoint=middlePoint(pt1,pt2);
	*pp=MiddlePoint;
	dist = distancePoint(pt1,pt2);
	x0 = pp->x;
	y0 = pp->y;
	++p->len;

	return MiddlePoint;
}
