/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include "Function.h"

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node in front of the list */

void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* Given a reference (pointer to pointer) to the head of a list and a key, deletes the first occurrence of key in linked list */

void deleteNodes(struct Node** head_ref, int key)
{
	// Store head node
	struct Node *temp = *head_ref, *prev;
	// If head node itself holds the key to be deleted.
	if (temp != NULL && temp->data == key){
		*head_ref = temp->next; // Change head
		free(temp); // Free old head
		return;
	}
	// Search for the key to be deleted, keep track of the previous node as we need to change 'prev->next'
	while(temp != NULL && temp->data != key){
		prev = temp;
		temp = temp->next;
	}
	// If key was not present in linked list
	if(temp == NULL)
	{
		return;
	}
	// Unlink the node from linked list
	prev->next = temp->next;
	free(temp); // Free memory
}

/* This function prints contents of linked list starting from the given node */
void printList(struct Node* n)
{
	while (n != NULL){
		printf(" %d ",n->data);
		n=n->next;
	}
}

