/*
 * Function13.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "Function.h"

int main()
{
	/* Start with the empty list */

	struct Node* head = NULL;

	push(&head, 7);
	push(&head, 1);
	push(&head, 3);
	push(&head, 2);

	puts("Created Linked List: ");
	printList(head);
	deleteNodes(&head, 1);
	puts("\nLinked List after Deletion of 1: ");
	printList(head);
	return 0;
}
