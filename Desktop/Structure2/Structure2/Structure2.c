/*
 * Structure2.c
 *
 *  Created on: Nov 7, 2021
 *      Author: narima
 */

#include<stdio.h>

struct point {int x;int y;};
struct point1 {float x; float y;};
struct point1 calMiddlePoint(struct point,struct point);

int main()
{
	struct point pt1,pt2;
	struct point1 middlePoint;
	pt1.x=1;pt1.y=1;
	pt2.x=2;pt2.y=2;
	middlePoint=calMiddlePoint(pt1,pt2);
}

struct point1 calMiddlePoint(struct point Pt1, struct point Pt2)
{
	struct point1 temp;
	temp.x = (float)(Pt1.x+Pt2.x)/2;
	temp.y = (float)(Pt1.y+Pt2.y)/2;
	return temp;
}
