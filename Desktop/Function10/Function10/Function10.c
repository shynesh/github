/*
 * Function10.c
 *
 *  Created on: Jan 9, 2022
 *      Author: narima
 */
#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include "Function.h"

static char configReceive[100][150] = {" "};
static int countT = 0;

int main()
{
	int len,i;
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;

	doc = xmlReadFile("sampleXml.xml",NULL,0);
	root_element = xmlDocGetRootElement(doc);
	get_child(root_element,1);
	int len2 = strlen(test);
	printf("%d\n",len2);
	(test) = "Hello";
	//(test+1) = "Hello2";
	len2 = strlen(test);
	printf("%d\n",len2);
//	for (i=0; i<len2;i++)
//	{
//		printf("%s\n",test[i]);
//	}

	len = strlen(configReceive);

	for(i=0;i<len;i++)
	{
		printf("%s\n",configReceive[i]);
	}
	xmlFreeDoc(doc);
	xmlCleanupParser();
}

void get_child(xmlNode *node,int indent_len)
{
	while(node)
	{
		if ((node->type == XML_ELEMENT_NODE) && (is_leaf(node)==1))
		{
			strcpy(configReceive[countT],xmlNodeGetContent(node));
			countT = countT + 1;
		}
		get_child(node->children, indent_len + 1);
		node = node->next;
	}
}
