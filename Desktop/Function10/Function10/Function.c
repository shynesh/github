/*
 * Function10.c
 *
 *  Created on: Jan 8, 2022
 *      Author: narima
 */
//static char configReceive[100][150] = {" "};
//static int countT = 0;

#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include "Function.h"



int is_leaf(xmlNode * node)
{
	//test = "Testing";
	xmlNode * child = node->children;
	while(child)
	{
		if (child->type == XML_ELEMENT_NODE)
		{
			return 0;
		}
		child = child->next;
	}
	return 1;
}



