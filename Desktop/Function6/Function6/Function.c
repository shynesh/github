/*
 * Function.c
 *
 *  Created on: Dec 27, 2021
 *      Author: narima
 */

#include <stdio.h>
#include <libxml/parser.h>
#include "Function.h"

int is_leaf(xmlNode * node)
{
	xmlNode * child = node->children;
	while(child)
	{
		if(child->type == XML_ELEMENT_NODE) return 0;
		child = child->next;
	}
	return 1;
}

void print_xml(xmlNode * node, int indent_len)
{
	while(node)
	{
		if((node->type == XML_ELEMENT_NODE)&&(is_leaf(node)==1))
		{
			//printf("%*c%s:%s\n",indent_len*2, '-', node->name,is_leaf(node)?xmlNodeGetContent(node):xmlGetProp(node, "id"));
			printf("%*c%s:%s\n",indent_len*2, '-', node->name,xmlNodeGetContent(node));
		}
		print_xml(node->children, indent_len + 1);
		node = node->next;
	}
}
