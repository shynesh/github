/*
 * Function.h
 *
 *  Created on: Dec 27, 2021
 *      Author: narima
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

int is_leaf(xmlNode * node);
void print_xml(xmlNode * node, int indent_len);

#endif /* FUNCTION_H_ */
