/*
 * Function6.c
 *
 *  Created on: Dec 27, 2021
 *      Author: narima
 */
#include <stdio.h>
#include <libxml/parser.h>
#include "Function.h"

int main()
{
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;

	doc = xmlReadFile("sampleXml.xml", NULL, 0);
	if (doc == NULL) {
		printf("Could not parse the XML file");
	}
	root_element = xmlDocGetRootElement(doc);
	print_xml(root_element, 1);
	xmlFreeDoc(doc);
	xmlCleanupParser();
}
