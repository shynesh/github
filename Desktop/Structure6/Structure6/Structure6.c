/*
 * Structure6.c
 *
 *  Created on: Jul 9, 2022
 *      Author: narima
 */

#include <stdio.h>

struct Personal {
		char * name;
		unsigned int Id;
	};

int main()
{

	struct Personal *person1;
	struct Personal person2 = { "Shynesh", 1};
	struct Personal *person3;
	person1=&person2;
	char * name;
	name = person1->name;
	UpdateDetails(person1);
	printf("%s\n",name);

}

void UpdateDetails(struct Personal *person)
{
	struct Personal person1 = { "Shyma" ,2};
	char * name;
	name=person->name;
	printf(">>%s\n",name);

	person = &person1;

	name=person->name;
	printf(">>%s\n",name);
}
