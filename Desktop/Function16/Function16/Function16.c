/*
 * Function16.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "Function.h"

int main()
{
	/* Start with the empty list */
	struct Node* head = NULL;

	/* Use push() to construct below list */
	/* 1->2->1->3->1 */

	push(&head, 1);
	push(&head, 3);
	push(&head, 1);
	push(&head, 2);
	push(&head, 1);

	/* Check the count function */
	printf("Count of nodes is %d", getCount(head));
	return 0;
}
