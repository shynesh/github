/*
 * Function.h
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

#ifndef NODE_VALUE
#define NODE_VALUE

struct Node {
		int data;
		struct Node* next;
};

#endif /* NODE_VALUE */


#endif /* FUNCTION_H_ */
