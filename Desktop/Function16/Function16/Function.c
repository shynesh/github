/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include "Function.h"

/* Given a reference (pointer to pointer) to the head of a list and an int, inserts a new node on the front of the list */
void push(struct Node ** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* Count no. of nodes in linked list */
int getCount(struct Node* head)
{
	int count =0; // Initialize count
	struct Node* current = head; // Initialize current
	while(current != NULL)
	{
		count++;
		current = current->next;
	}
	return count;
}

