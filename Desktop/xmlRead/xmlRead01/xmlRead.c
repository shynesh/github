/*
 * xmlRead.c
 *
 *  Created on: Dec 26, 2021
 *      Author: narima
 */


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<libxml/xmlreader.h>
#include<libxml/xmlmemory.h>
#include<libxml/parser.h>

extern char configReceive[7][80] = { " " };

//xml parsing
int main()
{
	int i;
	xmlParser1("/home/narima/Documents/eclipse-workspace/github/Desktop/xmlRead/xmlRead01/sampleXml.xml");
	for (i =0;i<7;i++)
	{
	printf("%s\n",configReceive[i]);
	}
	return 0;
}
int xmlParser1(char *filename)
{
	char *docname;
	xmlDocPtr doc;
	xmlNodePtr cur;
	xmlChar *uri;
	char config[7][80] = {"value1","value2","value3","value4","value5","value6","value7"};
	int count = 0;
	int count1 =0;
	docname = filename;
	doc = xmlParseFile(docname);
	cur = xmlDocGetRootElement(doc);
	cur = cur->xmlChildrenNode;
	while(cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"xmlConfig"))) {
			uri = xmlGetProp(cur, (xmlChar *)config[count++]);
			strcpy(configReceive[count1++], (char *)uri);
			xmlFree(uri);
		}
		cur = cur->next;
	}
	printf("%d\n",count);
	printf("%d\n",count1);
	count = 0;
	count1 =0;
	xmlFreeDoc(doc);
	return 0;
}
