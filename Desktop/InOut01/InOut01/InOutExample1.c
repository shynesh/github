/*
 * InOutExample1.c
 *
 *  Created on: Jul 9, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <ctype.h>

int main()
{
	int c;
	while((c=getchar()) != EOF)
	{
		putchar(tolower(c));
	}
	return 0;
}
