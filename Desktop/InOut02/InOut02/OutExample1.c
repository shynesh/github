/*
 * OutExample1.c
 *
 *  Created on: Jul 9, 2022
 *      Author: narima
 */

#include <stdio.h>

int main()
{
	char *s = "Hello, world";
	printf(":%15.12s:",s);
	// Total space will be 15 in between :, only print 12 character from variable s//
}
