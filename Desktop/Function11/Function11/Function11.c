/*
 * Function11.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */

#include <stdio.h>
#include <stdlib.h>
#include "Function.h"

// Program to create a simple linked list with 3 nodes

int main()
{
	struct Node* head = NULL;
	struct Node* second = NULL;
	struct Node* third = NULL;

	// allocate 3 nodes in the heap
	head = (struct Node*)malloc(sizeof(struct Node));
	second = (struct Node*)malloc(sizeof(struct Node));
	third = (struct Node*)malloc(sizeof(struct Node));

	head->data = 1; //assign data in the first node
	head->next = second; //Link first node with second node

	second->data = 2; //assign data to second node
	second->next = third; //Link second node with third node

	third->data = 3; //assign data to third node
	third->next = NULL;

	printList(head);

	return 0;
}
