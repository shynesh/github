/*
 * Function.c
 *
 *  Created on: Jan 17, 2022
 *      Author: narima
 */
#include <stdio.h>
#include "Function.h"

void printList(struct Node* n)
{
	while(n != NULL){
		printf("%d ",n->data);
		n=n->next;
	}
}
